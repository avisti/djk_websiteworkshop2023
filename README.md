# Introduktion til webprogrammering

Denne readme.md samler information omkring workshoppen på DJK. Her er links og referencer til alt vi har talt om i løbet af vores to dage sammen.  
Der er en PDF med alle slides i root folder. Den indeholder også en rækkel links...

#  

### INSTALLATIONER

Visual Studio Code: https://code.visualstudio.com/  
Filezilla: https://filezilla-project.org/  

#  

### RESSOURCER

_ Git. Daniel Shiffman: 1.1: Introduction - Git and GitHub for Poets  
__ https://www.youtube.com/watch?v=BCQHnlnPusY  
Gitlab Pages: https://docs.gitlab.com/ee/user/project/pages/  
Gitlab Pages custom domains (eget domænenavn på dine Pages)  
_ https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/  

Static site generators: F.eks:   
_ Eleventy.js -> https://www.11ty.dev/  
_ Astro -> https://astro.build/  

Værktøjer:  
_ https://www.fontsquirrel.com/tools/webfont-generator  
_ https://fonts.google.com/  
_ Brug Google web fonts uden at ramme Googles servere:   
__ https://gwfh.mranftl.com/fonts  
_ Galleri / lightbox -> wa-mediabox:  
__ https://github.com/jirihybek/wa-mediabox  

LINKS:  
_ https://www.artistsandhackers.org/  
_ https://solar.lowtechmagazine.com/about/the-solar-website/  
_ https://swirlkins-craft.shop/Seaside.html  
_ Everest Pipkin: https://anonymous-animal.neocities.org/  


Er der et værktøj der viser klimaaftryk for dit website!??  
_ Har ikke fundet noget? Nogen burde lave det ..  
_ Måske Joana Moll er i gang?: https://www.janavirgin.com/hidden_life.html  
